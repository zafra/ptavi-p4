#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = sys.argv[1]
PORT = int(sys.argv[2])
METHOD = sys.argv[3]
MESSAGE = sys.argv[4]
HEADER = sys.argv[5]

def main():
    if len(sys.argv) < 5:
        sys.exit("Error: at least four arguments are needed")

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            Request = METHOD.upper() + " sip:" + MESSAGE + " SIP/2.0\r\n" + "Expires: " + HEADER + "\r\n\r\n"
            my_socket.sendto(Request.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()

