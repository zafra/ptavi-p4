#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socketserver
import sys
import time
import json


class SIPRequest:

    def __init__(self, data):
        self.data = data.decode('utf-8')

    def parse(self):
        self._parse_command(self.data)
        headers = self.data.splitlines()[1:]
        self._parse_headers(headers)

    def _get_address(self, uri):
        self.uri = uri
        listuri = self.uri.split(":")
        schema = listuri[0]
        address = listuri[1]

        return address, schema

    def _parse_command(self, line):
        listdata = line.split()
        self.command = listdata[0].upper()
        self.uri = listdata[1]
        uri = self._get_address(self.uri)
        self.address = uri[0]
        schema = uri[1]

        if self.command == "REGISTER" and schema == "sip":
            self.result = "200 OK"
        elif self.command != "REGISTER":
            self.result = "405 Method Not Allowed"
        elif schema != "sip:":
            self.result = "416 Unsupported URI Scheme"

    def _parse_headers(self, first_nl):
        self.headers = {}
        for header in first_nl:
            if len(header) > 0:
                head = header.split(":")
                header1 = head[0]
                value = head[1].replace(" ", "")
                self.headers[header1] = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    IPAddress = {}

    def process_register(self, sip_request):
        if sip_request.headers["Expires"] == "0" and self.IPAddress != {}:
            self.IPAddress.pop(sip_request.address)
        elif sip_request.headers["Expires"] == "0":
            self.IPAddress = self.IPAddress
        else:
            expirestime = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                        time.gmtime(time.time()))
            self.IPAddress[sip_request.address] = {
                "address": self.client_address[0], "expires": expirestime
            }
        self.registered2json()

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]

        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)

    def registered2json(self):
        with open("registered.json", "w") as file:
            json.dump(self.IPAddress, file, indent=4)

    def json2registered(self):
        try:
            with open('registered.json', 'r') as file:
                self.IPAddress = json.load(file)
        except BaseException as exp:
            print(f"Error reading registered.json (exception: {exp})")
            self.IPAddress = {}


def main():
    PORT = int(sys.argv[1])

    if len(sys.argv) < 2:
        sys.exit("Error: at least 1 argument is needed")

    # Listens at port (my address)
    # and calls the SIPRegisterHandlers class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
